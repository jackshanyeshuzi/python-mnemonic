from mnemonic import Mnemonic
mnemo = Mnemonic("english")

while True:
    words = mnemo.generate_rand(strength=128)
    w = words.split(" ")
    temp = w[1]
    w[1] = w[0]
    w[0] = temp
    new_w = " ".join(w)
    result = mnemo.check(new_w)
    if not result:
        continue

    temp = w[11]
    w[11] = w[10]
    w[10] = temp
    new_w = " ".join(w)
    result = mnemo.check(new_w)
    if result:
        print(new_w)
        break